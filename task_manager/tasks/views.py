from cassandra.cqlengine import connection
from cassandra.cqlengine.management import sync_table
from cassandra.cluster import Cluster

from .models import Task

from django.http import JsonResponse
from django.views.decorators.http import require_GET
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.views import View

import json


@require_GET
def get_tasks(request, weekday):
    weekday_tasks = Task.objects.filter(
        weekday=weekday)

    unsorted = []
    for task in weekday_tasks:
        unsorted.append(task)
    
    unsorted.sort(key=lambda task: (task.start_time, task.end_time))
    
    results = []
    for task in unsorted:
        results.append(dict(task))

    response = {
        'count': weekday_tasks.count(),
        'results': results
    }

    return JsonResponse(response)


class TaskObjectView(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request):
        try :
            data = json.loads(request.body.decode('utf-8'))
            task = Task()
            for field in data:
                task[field] = data[field]

            task.save()

            return JsonResponse(dict(task), status=201)
        except Exception as exc:
            print(str(exc))
            return JsonResponse({
                'error': str(exc)
            }, status=400)

    def delete(self, request):
        data = json.loads(request.body.decode('utf-8'))
        task_id = data['task_id']

        task_results = Task.objects.filter(task_id=task_id)

        if task_results.count() == 1:
            task_results.delete()
            return JsonResponse({}, status=204)
        else:
            return JsonResponse({}, status=404)
