from django.urls import path
from . import views

urlpatterns = [
    path('', views.TaskObjectView.as_view()),
    path('<str:weekday>/', views.get_tasks, name='get_tasks'),
]
