import uuid
from cassandra.cqlengine import columns, ValidationError
from cassandra.cqlengine.models import Model

import re
import json


class Task(Model):
    DAYS = ['Monday', 'Tuesday', 'Wednesday',
            'Thursday', 'Friday', 'Saturday', 'Sunday']

    TIME_PATTERN = re.compile(r'^(?P<hour>\d{2}):(?P<minute>\d{2})$')

    task_id = columns.UUID(primary_key=True, default=uuid.uuid4)
    description = columns.Text(required=True)
    weekday = columns.Text(required=True, index=True)
    start_time = columns.Text(primary_key=True, required=True)
    end_time = columns.Text(primary_key=True, required=True)

    def validate_weekday(self):
        if self.weekday not in Task.DAYS:
            raise ValidationError('Weekday invalid {}.'.format(self.weekday))

    def validate_time(self, time):
        result = Task.TIME_PATTERN.match(time)

        if result:
            hour = int(result.group('hour'))
            minute = int(result.group('minute'))

            if hour < 0 or hour > 23 or minute < 0 or minute > 59:
                raise ValidationError('Invalid Time Format.')
        else:
            raise ValidationError('Invalid Time Format.')

    def validate_time_range(self):
        start = Task.TIME_PATTERN.match(self.start_time)
        start_hour = int(start.group('hour'))
        start_minute = int(start.group('minute'))

        end = Task.TIME_PATTERN.match(self.end_time)
        end_hour = int(end.group('hour'))
        end_minute = int(end.group('minute'))

        if start_hour > end_hour or (start_hour == end_hour and start_minute > end_minute):
            raise ValidationError(
                'Time Range Invalid {}:{} - {}:{}.'.format(start_hour, start_minute, end_hour, end_minute))

    def validate(self):
        self.validate_weekday()

        self.validate_time(self.start_time)
        self.validate_time(self.end_time)

        self.validate_time_range()

    @property
    def json(self):
        return json.dumps(dict(self))
